Minimization
------------
NIFTY provides several minimization routines.


.. toctree:: 
    :maxdepth: 1

    conjugate_gradient
    descent_minimizer

