.. currentmodule:: nifty

The ``ConjugateGradient`` class -- Minimization routine
.......................................................

.. autoclass:: ConjugateGradient
    :show-inheritance:
    :members:
