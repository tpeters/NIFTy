.. currentmodule:: nifty

The ``LMGLTransformation`` class -- A transformation routine
............................................................


.. autoclass:: LMGLTransformation
    :show-inheritance:
    :members:
