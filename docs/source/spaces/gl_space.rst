.. currentmodule:: nifty

The ``GLSpace`` class -- Gauss-Legendre pixelization of the sphere
..................................................................

.. autoclass:: GLSpace
    :show-inheritance:
    :members:
