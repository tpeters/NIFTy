.. currentmodule:: nifty

The ``HPSpace`` class -- HEALPix discretization of the sphere
.............................................................


.. autoclass:: HPSpace
    :show-inheritance:
    :members:
