.. currentmodule:: nifty

The ``LMSpace`` class -- Spherical Harmonics components
.......................................................

.. autoclass:: LMSpace
    :show-inheritance:
    :members:
