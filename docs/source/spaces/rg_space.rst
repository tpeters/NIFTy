.. currentmodule:: nifty

The ``RGSpace`` class -- Regular Cartesian grids
................................................

.. autoclass:: RGSpace
    :show-inheritance:
    :members:
