.. currentmodule:: nifty

The ``HPLMTransformation`` class -- A transformation routine
............................................................


.. autoclass:: HPLMTransformation
    :show-inheritance:
    :members:
