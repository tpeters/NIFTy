.. currentmodule:: nifty

The ``ResponseOperator`` class -- NIFTy ResponseOperator (example)
..................................................................

.. autoclass:: ResponseOperator
    :show-inheritance:
    :members:
