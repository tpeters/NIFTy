.. currentmodule:: nifty

The ``ProjectionOperator`` class -- NIFTY class for projection operators
........................................................................

.. autoclass:: ProjectionOperator
    :show-inheritance:
    :members:
