.. currentmodule:: nifty

The ``DiagonalOperator`` class -- NIFTY class for diagonal operators
....................................................................

.. autoclass:: DiagonalOperator
    :show-inheritance:
    :members:
