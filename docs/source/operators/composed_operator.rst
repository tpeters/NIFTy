.. currentmodule:: nifty

The ``ComposedOperator`` class -- NIFTY class for composed operators
....................................................................

.. autoclass:: ComposedOperator
    :show-inheritance:
    :members:
