.. currentmodule:: nifty

The ``Field`` class -- Basic class to define fields over spaces
...............................................................

The discrete representation of a continuous field over multiple spaces.

In NIFTY, Fields are used to store data arrays and carry all the needed metainformation (i.e. the domain) for operators to be able to work on them. In addition Field has methods to work with power-spectra.

.. autoclass:: Field
    :show-inheritance:
    :members:
