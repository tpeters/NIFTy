Two step creation of webpages:

sphinx-apidoc -l -e -d 3 -o sphinx/source/mod/ nifty/ nifty/plotting/ nifty/spaces/power_space/power_indices.py nifty/spaces/power_space/power_index_factory.py nifty/config/ nifty/basic_arithmetics.py nifty/nifty_meta.py nifty/random.py nifty/version.py  nifty/field_types/ nifty/operators/fft_operator/transformations/rg_transforms.py


creates all .rst files neccesary for ModuleIndex excluding helper modules

sphinx-build -b html sphinx/source/ sphinx/build/

generates html filel amd build directory
