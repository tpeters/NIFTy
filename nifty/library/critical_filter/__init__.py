# -*- coding: utf-8 -*-

from .critical_power_curvature import CriticalPowerCurvature
from .critical_power_energy import CriticalPowerEnergy
