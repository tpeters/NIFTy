# -*- coding: utf-8 -*-

from .hpmollweide import HPMollweide
from .glmollweide import GLMollweide
from .heatmap import Heatmap
