from .figure_2D import Figure2D
from .figure_3D import Figure3D
from .multi_figure import MultiFigure
